## Backend Coding Test

# Build a "Currency Exchange App"

1. Conversion API

    Implement a conversion API to allow providing an amount with specific currency and show converted values in all other currencies.

1. Exchange Rate History API

    Implement an API to get historical exchange rates of the last 30 days for a certain currency against all other currencies.

1. Dockerize the application

    Define docker configuration to package and isolate the application for local development

1. Package and deploy the application

    Use the best possible way to CI/CD and deploy your application, it is recommended to either do the actual deployment or write out your deployment plan.

## Dependencies

- Foreign exchange rates API: https://exchangeratesapi.io/

## Include & Usages

- Unit test & API feature tests `vendor/bin/phpunit`

- UAT environment: http://yhw.heliohost.us/currency/

    API Available: 

    1. /api/v1/conversion/{currency}, e.g. http://yhw.heliohost.us/currency/api/v1/conversion/USD

    1. /api/v1/history/{currency}, e.g. http://yhw.heliohost.us/currency/api/v1/history/USD

    - Restrictions

    1. `{currency}` should be in form **alphanumeric** of **length 3**

    1. API, Rate History, date counts **includes weekend** days
