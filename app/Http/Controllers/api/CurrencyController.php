<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\api\RequestBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Exception;

class CurrencyController extends RequestBaseController
{
    public function currency(Request $request, $currency = null, $history = false)
    {
        try
        {
            // Init
            $json = [];
            $responseStatus = 400;

            // Check param, currency
            if (!$this->checkCurrency($currency)) return response()->json($this->patchJson([
                'type'=>'error',
                'message'=>$this->messages['invalid-currency'],
                'status'=>400
            ]), 400);

            // Convert to uppercase
            $api = 'https://api.exchangeratesapi.io/';
            $action = 'latest';
            $currency = strtoupper($currency);

            // Parse history data
            if (!empty($history))
            {
              $action = 'history';
              $today = date('Y-m-d');
              $start = date('Y-m-d', strtotime("$today -1 month")); // a month before
              $end = date('Y-m-d', strtotime("$today -1 day")); // a day before
              $history = "&start_at=$start&end_at=$end";
            }
            else $history = '';

            // Build path
            $api = "$api$action?base=$currency$history";
            $response = Http::get($api);

            if (!$this->checkResponse($response)) throw new Exception($this->messages['request-error']);

            // Get body json
            $result = $response->json();

            if (!empty($result) && !empty($result['rates'])) return response()->json($this->patchJson([
                'type'=>'success',
                'data'=>$result['rates'],
                'status'=>200
            ]), 200);

            return response()->json($this->patchJson($json), $responseStatus);
        }
        catch (Exception $exception)
        {
            $json['message'] = $exception->getMessage() ? $exception->getMessage() : $this->messages['bad-connection'];
            return response()->json($this->patchJson($json), $responseStatus);
        }
    }

    public function history(Request $request, $currency = null)
    {
        return $this->currency($request, $currency, true);
    }
}
