<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RequestBaseController extends Controller
{
    private $jsonType = [
        'success',
        'error',
    ];

    public $messages = [
        'bad-request'=>'Bad request',
        'request-error'=>'Request error',
        'invalid-currency'=>'Invalid currency',
        'bad-connection'=>'Bad connection - external api',
    ];

    /*
     *  Build json data structure
     *  Ref: Google JSON guide
     */
    public function patchJson($options = [])
    {
        // Read options
        extract($options);

        $result = [];

        if (empty($type) || !is_string($type) || !in_array($type, $this->jsonType)) $type = 'error';

        if ($type == 'success') $result['data'] = empty($data) ? [] : $data;
        if ($type == 'error') $result['error'] = [
            'code'=>empty($status) ? 400 : $status,
            'message'=>empty($message) ? $this->messages['bad-request'] : $message,
        ];

        return $result;
    }

    /*
     *  Helpers
     */

    public function checkResponse($response)
    {
        return !(
            $response->failed()
            || $response->serverError()
            || $response->clientError()
        );
    }

    public function checkCurrency($currency = null)
    {
        if (
           !empty($currency)
           && strlen($currency) == 3
           && ctype_alpha(strtoUpper($currency))
        ) return true;

        return false;
    }
}
