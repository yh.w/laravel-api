<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\CurrencyController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->get('/conversion/{currency?}', [CurrencyController::class, 'currency']);
Route::middleware('api')->get('/history/{currency?}', [CurrencyController::class, 'history']);
Route::fallback(function(){ return response()->json(['message' => 'Not Found.'], 404); }); // Handle exceptions
