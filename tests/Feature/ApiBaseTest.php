<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Http\Response;

class ApiBaseTest extends TestCase
{
    public $validApiPrefix = 'api';
    public $invalidApiPrefix = 'apis';
    public $validApiVer = 'v1';
    public $invalidApiVer = 'v2';
    public $invalidApi = 'test';
    public $validApi = 'conversion';
    public $currency = 'USD';
    // Response:: Symfony\Component\HttpFoundation\Response

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAccessValidPrefix()
    {
        $this->json('get', "$this->validApiPrefix/$this->validApiVer/$this->validApi/$this->currency")
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure();
    }

    public function testAccessInvalid()
    {
        $this->json('get', "$this->invalidApiPrefix/$this->invalidApiVer/$this->invalidApi")
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testAccessInvalidPrefix1()
    {
        $this->json('get', "$this->invalidApiPrefix/$this->validApiVer/$this->validApi")
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testAccessInvalidPrefix2()
    {
        $this->json('get', "$this->invalidApiPrefix/$this->invalidApiVer/$this->validApi")
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testAccessInvalidPrefix3()
    {
        $this->json('get', "$this->invalidApiPrefix/$this->validApiVer/$this->invalidApi")
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testAccessInvalidApiVer()
    {
        $this->json('get', "$this->validApiPrefix/$this->invalidApiVer/$this->validApi")
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testAccessInvalidApi()
    {
        $this->json('get', "$this->validApiPrefix/$this->invalidApiVer/$this->invalidApi")
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
