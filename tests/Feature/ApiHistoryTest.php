<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Http\Response;

class ApiHistoryTest extends TestCase
{
    public $apiPrefix = 'api/v1';
    public $api = 'history';
    // Response:: Symfony\Component\HttpFoundation\Response

    public $currencyValidUpper = 'USD';
    public $currencyValidLower = 'usd';
    public $currencyInvalidNonCurrency = 'qqq';
    public $currencyInvalidEmpty = '';
    public $currencyInvalidChar1 = 'a';
    public $currencyInvalidChar2 = 'bc';
    public $currencyInvalidInt = '123';
    public $currencyInvalidSym = '%^&';
    public $currencyInvalidMix = '*a5';

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAccessBase()
    {
        $this->json('get', "$this->apiPrefix/$this->api")
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJson(["error"=>["code"=>400,"message"=>"Invalid currency"]]);
    }

    public function testAccessCurrencyValidUpper()
    {
        $this->json('get', "$this->apiPrefix/$this->api/$this->currencyValidUpper")
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure();
    }

    public function testAccessCurrencyValidLower()
    {
        $this->json('get', "$this->apiPrefix/$this->api/$this->currencyValidLower")
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure();
    }

    public function testAccessCurrencyInvalidNonCurrency()
    {
        $this->json('get', "$this->apiPrefix/$this->api/$this->currencyInvalidNonCurrency")
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJson(["error"=>["code"=>400,"message"=>"Request error"]]);
    }

    public function testAccessCurrencyInvalidEmpty()
    {
        $this->json('get', "$this->apiPrefix/$this->api/$this->currencyInvalidEmpty")
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJson(["error"=>["code"=>400,"message"=>"Invalid currency"]]);
    }

    public function testAccessCurrencyInvalidChar1()
    {
        $this->json('get', "$this->apiPrefix/$this->api/$this->currencyInvalidChar1")
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJson(["error"=>["code"=>400,"message"=>"Invalid currency"]]);
    }

    public function testAccessCurrencyInvalidChar2()
    {
        $this->json('get', "$this->apiPrefix/$this->api/$this->currencyInvalidChar2")
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJson(["error"=>["code"=>400,"message"=>"Invalid currency"]]);
    }

    public function testAccessCurrencyInvalidInt()
    {
        $this->json('get', "$this->apiPrefix/$this->api/$this->currencyInvalidInt")
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJson(["error"=>["code"=>400,"message"=>"Invalid currency"]]);
    }

    public function testAccessCurrencyInvalidSym()
    {
        $this->json('get', "$this->apiPrefix/$this->api/$this->currencyInvalidSym")
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJson(["error"=>["code"=>400,"message"=>"Invalid currency"]]);
    }

    public function testAccessCurrencyInvalidMix()
    {
        $this->json('get', "$this->apiPrefix/$this->api/$this->currencyInvalidMix")
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJson(["error"=>["code"=>400,"message"=>"Invalid currency"]]);
    }
}
