<?php

namespace Tests\Unit;

//use PHPUnit\Framework\TestCase;
use Tests\TestCase;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\api\RequestBaseController;

class ApiBaseControllerTest extends TestCase
{
    public $controller = null;
    public $validApi = 'api/v1/conversion/usd';
    public $invalidApi = 'api/v2/conversion';

    public function setUp(): void
    {
        parent::setUp();

        $this->controller = app()->make(RequestBaseController::class);

        $domain = $_ENV['APP_URL'];
        $this->validApi = "$domain/$this->validApi";
        $this->invalidApi = "$domain/$this->invalidApi";
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testPatchJson()
    {
        // Mock $options
        $options = [];
        $resultError = ['error'=>['code'=>400, 'message'=>'Bad request']];
        $resultSuccess = ['data'=>[]];

        $this->assertEquals($this->controller->patchJson($options), $resultError);

        $options = ['type'=>'error', 'code'=>400, 'message'=>'Bad request'];
        $this->assertEquals($this->controller->patchJson($options), $resultError);

        $options = ['type'=>'success', 'data'=>[]];
        $this->assertEquals($this->controller->patchJson($options), $resultSuccess);
    }

    public function testCheckResponse()
    {
        $response = Http::get($this->invalidApi);
        $this->assertFalse($this->controller->checkResponse($response));
    }

    public function testCheckCurrency()
    {
        $this->assertFalse($this->controller->checkCurrency(''));
        $this->assertFalse($this->controller->checkCurrency('1'));
        $this->assertFalse($this->controller->checkCurrency('12'));
        $this->assertFalse($this->controller->checkCurrency('123'));
        $this->assertFalse($this->controller->checkCurrency('%'));
        $this->assertFalse($this->controller->checkCurrency('%#'));
        $this->assertFalse($this->controller->checkCurrency('%#*'));
        $this->assertFalse($this->controller->checkCurrency('a'));
        $this->assertFalse($this->controller->checkCurrency('ab'));
        $this->assertTrue($this->controller->checkCurrency('abc'));
    }
}
